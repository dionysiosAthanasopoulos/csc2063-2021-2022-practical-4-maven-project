package GUI;

import java.io.File;

import javax.swing.JFileChooser;

import Controller.StorageController;
import Controller.StorageType;


@SuppressWarnings("serial")
public class GUI extends javax.swing.JFrame{

	private static GUI GUI;


	public static void main(String[] args) {

		String userFolder = System.getProperty( "user.dir" );
		System.out.print( userFolder );

		JFileChooser chooser = new JFileChooser();
		chooser.setFileSelectionMode( JFileChooser.DIRECTORIES_ONLY );
		chooser.setCurrentDirectory( new File( userFolder ) );
		chooser.showOpenDialog( GUI );

		StorageController storageController = new StorageController();
		storageController.storage( StorageType.FILE );
	}
}