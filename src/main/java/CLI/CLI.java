package CLI;

import Controller.StorageController;
import Controller.StorageType;


public class CLI {

	public static void main(String[] args) {

		StorageController storageController = new StorageController();
		storageController.storage( StorageType.FILE );
	}
}